﻿using System;
using System.Web;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Resources;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuthFilter
{
    public class BaseResource
    {
		public BaseResource()
		{
            // TODO: configurable route root rather than using model type name convention
			_httpcontext = HttpContext.Current;
			// this is to persist type name through downcasts for later redirection
			ModelType = GetType();
			Loaded = Created = Updated = Deleted = false; // initialize action result propertiesa
			// moved Id to subclasses, could be Id or NewGuid...
			//Id = Guid.NewGuid(); // can be changed post-ctor
		}

		[ScaffoldColumn(false), NotMapped]
		public string StringId { get; set; } // subclasses should set this to stringified Id...

//		protected HttpContext _context { get; set; }
		protected HttpContext _httpcontext;
		//[Key]
		//public Guid Id { get; set; } // TODO: should Id be string or int?  thinking string.
        // TODO: turn Id to int?
        //public string Name { get; set; }

		// these properties are intended to be set by subclasses
		// to indicate success or failure of update/delete prior to calling base controller method
		// *NOTE*, default MVC4 scaffolding IGNORES ScaffoldColumn attribute!!!
		// adding T4 templates (customized or not) causes attribute to be respected.
		// http://stackoverflow.com/questions/12431238/mvc-scaffolding-with-custom-t4-templates-in-vs-2012-express
		// http://blog.stevensanderson.com/2011/04/06/mvcscaffolding-overriding-the-t4-templates/

		[ScaffoldColumn(false), NotMapped]
		public bool Created { get; set; }
		[ScaffoldColumn(false), NotMapped]
		public bool Deleted { get; set; }
		[ScaffoldColumn(false), NotMapped]
		public bool Updated { get; set; }
		[ScaffoldColumn(false), NotMapped]
		public bool Loaded { get; set; }

		// this is instead of GetType to allow retaining type info during a model downcast.
		[NotMapped]
		public Type ModelType { get; set; } // used for redirection to Model controller (uses convention of shared name)
		public IEnumerable<object> DummyModels { get; set; }
    }
}