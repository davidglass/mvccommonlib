﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuthFilter
{
	public class CachedResource : BaseResource
	{
		private Guid _id;

		[Key]
		public Guid Id {
			get
			{
				return _id;
			}
			set {
				_id = value;
				base.StringId = _id.ToString();
			}
		} // TODO: should Id be string or int?  thinking string.
		
		public CachedResource()
		{
			Id = Guid.NewGuid();
		}

		public Dictionary<Guid, object> List()
		{
			// initialize cache if necessary:
			_httpcontext.Cache[ModelType.Name] = _httpcontext.Cache[ModelType.Name] ?? new Dictionary<Guid, object>();
			var ccr = (Dictionary<Guid, object>)_httpcontext.Cache[ModelType.Name];
			if (ccr.Count < 1)
			{
				// model list is now empty...this populates it with DummyModels, loaded by controller if necessary:
				foreach (var model in DummyModels)
				{
					ccr[((CachedResource)model).Id] = model;
				}
			}
			return ccr;
		}

		// TODO: LoadedProperties collection? rename Load to LoadProperties ?
		// convert this to interface?
		public virtual void Load()
		{
		}

		public bool Create()
		{
			if (_httpcontext.Cache[ModelType.Name] == null)
			{
				_httpcontext.Cache[ModelType.Name] = new Dictionary<Guid, CachedResource>();
			}
			var resources = (Dictionary<Guid, object>)_httpcontext.Cache[ModelType.Name];
			//Id = Guid.NewGuid(); moved to ctor
			resources[Id] = this;
			Created = true;
		    return Created;
		}

		public bool Update()
		{
			// TODO: consider fully-qualified type name as key to avoid namespace conflicts:
			((Dictionary<Guid, object>)_httpcontext.Cache[ModelType.Name])[Id] = this;
			Updated = true;
			return Updated;
		}

		// NOTE, CRD actions return bool in addition to setting property for BaseResourceController.
		public bool Delete()
		{
			// remove self from cache:
			Deleted = ((Dictionary<Guid, object>)_httpcontext.Cache[ModelType.Name]).Remove(Id);
			return Deleted;
		}
	}
}