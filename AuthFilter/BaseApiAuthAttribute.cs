﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace AuthFilter
{
    public class BaseApiAuthAttribute : System.Web.Http.AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
        }

        protected override bool IsAuthorized(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var bap = new BaseAuthorizationProvider(actionContext.ControllerContext.RouteData.Values["controller"].ToString()); // e.g. "par"
            var action = actionContext.ActionDescriptor.ActionName; // e.g. "Post", "Get", "Put", "Delete", "ParEmp", "ParAttachment", etc.
            // TODO: consider separate Attachment permissions depending on Method???
            // *NOTE*, Api/AttachmentController currently handles Attachment actions other than POST.
            var method = actionContext.Request.Method;

            return (
                (method == HttpMethod.Post && bap.Can(ModelAction.Create))
                || (method == HttpMethod.Get) && bap.Can(ModelAction.Read)
                || action.Equals("Put") && bap.Can(ModelAction.Update)
                || action.Equals("Delete") && bap.Can(ModelAction.Delete)
            );
            //return base.IsAuthorized(actionContext);
        }

        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            //actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            //actionContext.Response.ReasonPhrase = "Your permissions are inadequate to perform the requested action.";
            //actionContext.Response.StatusCode = System.Net.HttpStatusCode.Forbidden;
//            actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(
            actionContext.Response = actionContext.Request.CreateErrorResponse(
//                System.Net.HttpStatusCode.Unauthorized, // Unauthorized response triggers login box on IE8
                System.Net.HttpStatusCode.InternalServerError,
                "Your permissions are inadequate for the attempted action."
            );
            //base.HandleUnauthorizedRequest(actionContext);
        }
    }
}
