﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace AuthFilter
{
    public class BaseAuthorizationProvider
    {
        protected readonly Dictionary<string, ActionFlags> Actions = new Dictionary<string, ActionFlags>();
        //protected readonly HttpContextBase Context;
        protected readonly HttpVerbs Httpverb;
        protected readonly string Action;
        protected readonly string Controller;
        //private readonly string _id; // specific record id, if provided
        protected readonly Dictionary<HttpVerbs, ModelAction> VerbAction = new Dictionary<HttpVerbs, ModelAction> {
            { HttpVerbs.Post, ModelAction.Create },
            { HttpVerbs.Get, ModelAction.Read },
            { HttpVerbs.Put, ModelAction.Update },
            { HttpVerbs.Delete, ModelAction.Delete }
        };

        public BaseAuthorizationProvider(HttpContextBase hcb)
        {
            //Context = hcb;
            var rdv = ((MvcHandler)hcb.CurrentHandler).RequestContext.RouteData.Values;
            Httpverb = (HttpVerbs)(Enum.Parse(typeof(HttpVerbs), hcb.Request.GetHttpMethodOverride(), true)); // returns actual method if no override specified.
            // assumes specific Resources will be accessed by Id (see BaseResourceModel)
            // _id = rdv.ContainsKey("Id") ? rdv["Id"].ToString() : "";
            Controller = rdv["controller"].ToString();
            Action = rdv["action"].ToString();
            //var n = _context.User.Identity.Name;
            Init();
        }

        public BaseAuthorizationProvider(string ctl)
        {
            Controller = ctl;
            Init();
        }

        private void Init()
        {
            var af = new ActionFlags();
            var cx = HttpContext.Current;
            //var connstr = Context.Application["connstr"].ToString();
            var connstr = cx.Application["connstr"].ToString();

            // for now, assuming Controller name and Application name match:
            using (System.Data.SqlClient.SqlConnection c = new System.Data.SqlClient.SqlConnection(connstr))
            {
                c.Open();
                string LanId = cx.User.Identity.Name;
                // *skip impersonation for Impersonation Controller itself:*
                if (Controller != "impersonation")
                {
                    var cmd1 = new SqlCommand()
                    {
                        CommandType = System.Data.CommandType.StoredProcedure,
                        CommandText = "GetUserInfoNew", // TODO: make this configurable?
                        Connection = c
                    };
                    cmd1.Parameters.Add(
                        // TODO: use LanId here???
                        //new SqlParameter("UserId", 2)
                        new SqlParameter("LanId", cx.User.Identity.Name)
                        );
                    var r1 = cmd1.ExecuteReader(System.Data.CommandBehavior.SingleResult);
                    if (r1.HasRows)
                    {
                        r1.Read();
                        LanId = r1.GetString(2); // impersonated LanId or own if not impersonating...
                    }
                    r1.Close();
                }
                var cmd = new SqlCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "GetAppPermissions", // TODO: make this configurable?
                    Connection = c
                };
                cmd.Parameters.Add(
                    // TODO: use LanId here???
                    //new SqlParameter("UserId", 2)
//                    new SqlParameter("LanId", cx.User.Identity.Name)
                    new SqlParameter("LanId", LanId)
                    );
                cmd.Parameters.Add(
                    new SqlParameter("AppName", Controller)
                );
                var r = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                if (r.HasRows)
                {
                    r.Read();
                    af = r.GetBoolean(0) ? af | ActionFlags.Create : af;
                    af = r.GetBoolean(1) ? af | ActionFlags.Read : af;
                    af = r.GetBoolean(2) ? af | ActionFlags.Update : af;
                    af = r.GetBoolean(3) ? af | ActionFlags.Delete : af;
                }
                r.Close();
            }

            //var appname = WebConfigurationManager.AppSettings["appName"];
            // initialize permissions from cached UserPermissions (test purposes only)...
            // NOTE, this must occur *BEFORE* any Controller actions.
            //Context.Cache["UserPermissions"] = Context.Cache["UserPermissions"] ?? new UserPermissions();
            //var up = (UserPermissions)Context.Cache["UserPermissions"];
            //var af = new ActionFlags();
            //af = up.PermitCreate ? af | ActionFlags.Create : af;
            //af = up.PermitRead ? af | ActionFlags.Read : af;
            //af = up.PermitUpdate ? af | ActionFlags.Update : af;
            //af = up.PermitDelete ? af | ActionFlags.Delete : af;
            Allow(Controller, af);
        }

        // set individual flag:
        public void Allow(string controller, ModelAction a)
        {
            Actions[controller] = Actions.ContainsKey(controller) ? Actions[controller] | (ActionFlags)a : (ActionFlags)a;
        }

        // sets all flags at once:
        public void Allow(string controller, ActionFlags af)
        {
            Actions[controller] = af;
        }

        // implicit ModelAction, based on HTTP verb:
        public bool Can()
        {
            // ties access to new/edit forms to Create/Update privileges:
            return Action.Equals("New") ? Can(ModelAction.Create)
                    : Action.Equals("Edit") ? Can(ModelAction.Update)
                    : Can(VerbAction[Httpverb]);
        }

        // explicit ModelAction:
        public bool Can(ModelAction ma)
        {
            return Actions.ContainsKey(Controller) && (Actions[Controller] & (ActionFlags)ma) > 0;
        }

        public void Disallow(string controller, ModelAction a)
        {
            if (Actions.ContainsKey(controller))
            {
                Actions[controller] = Actions[controller] ^ (ActionFlags)a;
            }
        }
    }

    // additional Permissions can be added as needed (e.g. based on controller action, impersonation, etc.),
    // but Can() logic will need to be modified if so.
    [Flags]
    public enum ActionFlags
    {
        Create = 1,
        Read = 2,
        Update = 4,
        Delete = 8
    }

    //// single-valued specific action:
    public enum ModelAction
    {
        Create = 1,
        Read = 2,
        Update = 4,
        Delete = 8
    }
}
